//
// Created by Yossi on 08/08/2022.
// This code follows Google C++ Style Guide.
//


#ifndef HOMEWORK_INCLUDE_GEOMETRY_HPP_
#define HOMEWORK_INCLUDE_GEOMETRY_HPP_

#include <vector>
#include <iostream>

namespace geometry {
struct Point {
  double x;
  double y;
  explicit Point(double x = 0, double y = 0) : x(x), y(y) {}
};

struct Vector {
  double x;
  double y;
  explicit Vector(double x = 0, double y = 0) : x(x), y(y) {}

  double cross(const Vector &other) const;
};

struct Segment {
  Point a;
  Point b;
  Vector v;
  Segment(const Point &a, const Point &b);

  [[nodiscard]] std::pair<bool, geometry::Point> intersectsWith(const Segment &other) const;

  bool isPointToLeft(const Point &p) const;
};

struct Polygon {
  int id;
  Point center;
  std::vector<Point> points;
  std::vector<Segment> segments;

  double area;
  double min_x = std::numeric_limits<double>::max();
  double min_y = std::numeric_limits<double>::max();
  double max_x = std::numeric_limits<double>::lowest();
  double max_y = std::numeric_limits<double>::lowest();

  Polygon(int _id, std::vector<Point> _points);

  [[nodiscard]] bool isInside(const Point &point) const;

  friend std::ostream &operator<<(std::ostream &os, const Polygon &polygon) {
    os << "P: id: " << polygon.id << " points: " << polygon.points.size() << " area: " << polygon.area;
    return os;
  }

  Polygon getIntersection(const Polygon &other) const;

 private:
  void calculateArea();
  void findCenter();
  void orientPointsCCW();
  void updateMinMax(const Point &p);
};
}
#endif //HOMEWORK_INCLUDE_GEOMETRY_HPP_
