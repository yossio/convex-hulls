// This code follows Google C++ Style Guide.

#include <iostream>
#include <fstream>
#include <json.hpp>
#include <string>
#include <regex>
#include <unordered_set>

#include "geometry.hpp"
#include <opencv2/opencv.hpp>

#define SCALE 10
#define LIM 500

std::vector<geometry::Polygon> loadPolygons(const std::string &filename) {
  if (filename.empty()) {
    throw std::runtime_error("Filename cannot be empty");
  }
  std::ifstream f(filename);
  nlohmann::json data = nlohmann::json::parse(f);
  std::vector<geometry::Polygon> result;
  for (const auto &ch : data["convex hulls"]) {
    int id = ch["ID"].get<int>();
    std::vector<geometry::Point> points;
    for (const auto &p: ch["apexes"]) {
      points.emplace_back(p["x"].get<double>(), p["y"].get<double>());
    }
    result.emplace_back(id, points);
    std::cout << result.back() << std::endl;
//    if (result.size() > 3) {
//      break;
//    }
  }
  return result;
}

void dumpPolygons(const std::string &filename, const std::vector<geometry::Polygon> &polygons) {
  std::ofstream output(filename);
  if (not output) {
    printf("Failed to open '%s' for output.\n", filename.c_str());
    return;
  }
  nlohmann::json data;
  data["convex hulls"] = {};
  for (const auto &p:polygons) {
    nlohmann::json j_polygon;
    j_polygon["ID"] = p.id;
    j_polygon["apexes"] = {};
    for (const auto &apex: p.points) {
      nlohmann::json j_apex;
      j_apex["x"] = apex.x;
      j_apex["y"] = apex.y;
      j_polygon["apexes"].emplace_back(j_apex);
    }
    data["convex hulls"].emplace_back(j_polygon);
  }

  // Output the data in the desired format, indents of 3 s1paces and a space before the semicolon
  auto text_data = data.dump(3);
  text_data = std::regex_replace(text_data, std::regex(":"), " :");
  output << text_data << std::endl << std::endl;
}

std::vector<geometry::Polygon> filterPolygons(std::vector<geometry::Polygon> input_polygons) {
  std::unordered_map<int, int> overlapped_indices;
  for (size_t i = 0; i < input_polygons.size(); ++i) {
    auto &p1 = input_polygons[i];
    if (overlapped_indices.count(p1.id)) {
      printf("*** Skipping polygon #%d because it was already overlapped by %d\n", p1.id, overlapped_indices[p1.id]);
      continue;
    }
    double p1_area = p1.area;
    printf("*** Checking polygon #%d for intersection with others\n", p1.id);
    for (size_t j = i + 1; j < input_polygons.size(); ++j) {
      auto &p2 = input_polygons[j];
      if (overlapped_indices.count(j)) {
        printf("    Polygon #%d is already overlapped.\n", p2.id);
        continue;
      }
      printf("    Polygon #%d check: ", p2.id);

      auto intersection = p1.getIntersection(p2);
      if (intersection.points.empty()) {
        printf("...No intersection\n");
        continue;
      }
      double intersection_area = intersection.area;
      double p2_area = p2.area;
      printf(" Intersection area : %3.2f units; ", intersection_area);
      if (intersection_area * 2 > p2_area) {
        overlapped_indices[p2.id] = p1.id;
        printf("  ... >50%% of #%d's area overlapped", p2.id);
      }
      if (intersection_area * 2 > p1_area) {
        overlapped_indices[p1.id] = p2.id;
        printf("  ... >50%% of #%d's area overlapped\n", p1.id);
        break;
      }
      printf("\n");
    }
  }
  std::vector<geometry::Polygon> result;// = {input_polygons[0], input_polygons[2]};
  for (size_t i = 0; i < input_polygons.size(); ++i) {
    if (overlapped_indices.count(i) == 0) {
      result.emplace_back(input_polygons[i]);
    }
  }
  return result;
}

std::vector<geometry::Polygon> getIntersections(std::vector<geometry::Polygon> input_polygons) {
  std::vector<geometry::Polygon> result;
  for (size_t i = 0; i < input_polygons.size(); ++i) {
    auto &p1 = input_polygons[i];
    for (size_t j = i + 1; j < input_polygons.size(); ++j) {
      auto &p2 = input_polygons[j];
      auto intersection = p1.getIntersection(p2);
      if (intersection.points.empty()) {
        continue;
      }
      result.emplace_back(intersection);
    }
  }
  return result;
}

cv::Point2i p(geometry::Point p) {
  return cv::Point2i{
      static_cast<int>(LIM + SCALE * p.x),
      static_cast<int>(LIM + SCALE * p.y)
  };
}

struct Handler {
  bool draw_text = false; /// Modify to 'true' to draw IDs on the image
  Handler() {
    image = cv::Mat(LIM * 2, LIM * 2, CV_8UC3);
  }
  void drawPolygon(const geometry::Polygon &poly,
                   int thickness = 1,
                   const cv::Scalar &color = cv::Scalar(255, 255, 255)) {
    if (draw_text) {
      cv::putText(image, std::to_string(poly.id), p(poly.center),
                  cv::FONT_HERSHEY_SIMPLEX, 3, color);
    }
    for (size_t i = 0; i < poly.segments.size(); ++i) {
      const auto &segment = poly.segments[i];
      if (draw_text) {
        cv::putText(image, std::to_string(i), p(segment.a),
                    cv::FONT_HERSHEY_PLAIN, 1, color);
      }
      cv::line(image, p(segment.a), p(segment.b), color, thickness);
    }
  }
  void drawPolygons(const std::vector<geometry::Polygon> &polygons,
                    int thickness = 1,
                    cv::Scalar color = cv::Scalar(255, 255, 255)) {
    for (const auto &p : polygons) {
      drawPolygon(p, thickness, color);
    }
  }
  void showImage() {
    namedWindow("Display Image", cv::WINDOW_AUTOSIZE);
    imshow("Display Image", image);
    cv::waitKey(0);
  }
  cv::Mat image;
};

int main(int argc, char **argv) {
  if (argc < 2) {
    printf("You need to provide the argument with proper path to a JSON file\n\tE.g. ./main ../convex_hulls.json\n");
    return -1;
  }
  Handler handler;
  auto polygons = loadPolygons(argv[1]);
  double min_x = std::numeric_limits<double>::max();
  double min_y = std::numeric_limits<double>::max();
  double max_x = std::numeric_limits<double>::lowest();
  double max_y = std::numeric_limits<double>::lowest();
  for (const auto &p: polygons) {
    max_x = fmax(p.max_x, max_x);
    max_y = fmax(p.max_y, max_y);
    min_x = fmin(p.min_x, min_x);
    min_y = fmin(p.min_y, min_y);
  }
  auto intersections = getIntersections(polygons);
  auto filtered_polygons = filterPolygons(polygons);
  dumpPolygons("../result_convex_hulls.json", filtered_polygons);

  handler.drawPolygons(polygons, 1);
  handler.drawPolygons(filtered_polygons, 2, CV_RGB(50, 255, 0));
  handler.drawPolygons(intersections, 4, CV_RGB(255, 50, 0));
  handler.showImage();
  return 0;
}
