//
// Created by Yossi on 08/08/2022.
// This code follows Google C++ Style Guide.
//

#include "geometry.hpp"
#include <utility>
#include <cmath>
#include <algorithm>

double geometry::Vector::cross(const geometry::Vector &other) const {
  // Simplified version of 3D vector cross multiplication, checking Z value only
  return x * other.y - y * other.x;
}

geometry::Segment::Segment(const geometry::Point &a, const geometry::Point &b) : a(a), b(b) {
  v = Vector{b.x - a.x, b.y - a.y};
}

std::pair<bool, geometry::Point> geometry::Segment::intersectsWith(const geometry::Segment &other) const {
  /// Please use a viewer with unicode support to see the illustration properly
  /// Two segments intersect only if:
  ///  For each segment, each one of the other segment's points lies on a different side
  /// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
  /// ⠀⠀⠀⠀⠀⠀⠀⠀⢀⣤⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
  /// ⠀⠀⠀⠀⠀⠀⠀⠀⠸⠿⠇⠄⠀⠀⠀this.a⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
  /// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⠐⠀⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
  /// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⠐⠠⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
  /// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠐⢀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣴⣶⡆⠀other.b⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
  /// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠁⠄⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⡀⠠⠊⠈⠉⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
  /// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⠐⠄⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⠄⠊⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
  /// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⠐⠠⠀⠀⠀⠀⡀⠔⠈⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
  /// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢑⢔⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
  /// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠄⠁⠀⠀⠀⠢⠄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
  /// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⡠⠐⠈⠀⠀⠀⠀⠀⠀⠀⠀⠈⠂⢄⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
  /// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⡠⠂⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠐⢄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
  /// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠠⠈⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠡⠄⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
  /// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⠠⠂⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⠂⢄⣀⣄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
  /// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠄⠈⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⠛⠛⠀this.b⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
  /// ⠀⠀⠀⠀⠀⠀⠀⠀⣠⣤⡔⠀⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
  /// ⠀⠀⠀⠀⠀⠀⠀⠀⠻⠟⠁⠀⠀other.a ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
  /// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀


  /// We test for each point whether it lies to left the given segment,

  bool other_a_on_left = this->isPointToLeft(other.a);
  bool other_b_on_left = this->isPointToLeft(other.b);
  bool this_a_on_left = other.isPointToLeft(this->a);
  bool this_b_on_left = other.isPointToLeft(this->b);

  /// Now we check where the 'other' segment lies on both sides of 'this' and the vice versa
  bool other_is_on_both_sides = other_a_on_left != other_b_on_left;
  bool this_is_on_both_sides = this_a_on_left != this_b_on_left;

  /// Intersection occurs only if:
  bool intersects = other_is_on_both_sides and this_is_on_both_sides;

  if (intersects) {
    /// Intersection point calculation taken from:
    /// https://en.wikipedia.org/wiki/Line%E2%80%93line_intersection#Given_two_points_on_each_line_segment
    const double &x1 = a.x;
    const double &x2 = b.x;
    const double &x3 = other.a.x;
    const double &x4 = other.b.x;
    const double &y1 = a.y;
    const double &y2 = b.y;
    const double &y3 = other.a.y;
    const double &y4 = other.b.y;

    double t_num = (x1 - x3) * (y3 - y4) - (y1 - y3) * (x3 - x4);
    double t_denom = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);
    double t = t_num / t_denom;

    return {intersects, Point{x1 + t * (x2 - x1), y1 + t * (y2 - y1)}};
  }

  return {intersects, Point{}};
}

bool geometry::Segment::isPointToLeft(const geometry::Point &p) const {
  Vector bp(p.x - b.x, p.y - b.y);
  return v.cross(bp) > 0;
}

geometry::Polygon::Polygon(int _id, std::vector<Point> _points) : id(_id), points(std::move(_points)) {
  findCenter();
  orientPointsCCW();
  calculateArea();

  for (size_t i = 0; i < points.size(); ++i) {
    auto &p = points[i];
    updateMinMax(p);

    // Create segments
    if (i == 0) {
      segments.emplace_back(points.back(), p);
    } else {
      segments.emplace_back(points[i - 1], p);
    }
  }
}

void geometry::Polygon::updateMinMax(const geometry::Point &p) {
  max_x = fmax(p.x, max_x);
  max_y = fmax(p.y, max_y);
  min_x = fmin(p.x, min_x);
  min_y = fmin(p.y, min_y);
}

void geometry::Polygon::orientPointsCCW() {
  std::sort(points.begin(), points.end(), [this](const Point &a, const Point &b) {
    double angle_a = atan2(a.y - center.y, a.x - center.x);
    double angle_b = atan2(b.y - center.y, b.x - center.x);
    return angle_a > angle_b;
  });
}

void geometry::Polygon::findCenter() {
  double sum_x{};
  double sum_y{};
  for (const auto &p: points) {
    sum_x += p.x;
    sum_y += p.y;
  }
  center = Point{sum_x / points.size(), sum_y / points.size()};
}

void geometry::Polygon::calculateArea() {
  /// Using the Shoelace formula  https://en.wikipedia.org/wiki/Shoelace_formula#Shoelace_formula
  double sum{};
  // We start from the second point
  for (size_t i = 1; i < points.size(); ++i) {
    auto &prev_p = points[i - 1];
    auto &this_p = points[i];
    sum += prev_p.x * this_p.y - prev_p.y * this_p.x;
  }
  area = std::fabs(0.5 * sum);
}

bool geometry::Polygon::isInside(const geometry::Point &point) const {
  /// First we screen out the points that are outside the bounding box
  if (point.x > max_x or point.x < min_x or point.y > max_y or point.y < min_y) {
    return false;
  }
  /// If the point is inside the bounding box, we cast a ray right,
  /// and then check whether the number of intersections is odd.
  double dx = max_x - min_x;
  const Point far_right{max_x + dx, point.y};
  Segment ray(point, far_right);

  size_t intersect_count{};
  for (const auto &segment: segments) {
    auto[intersects, point] = ray.intersectsWith(segment);
    if (intersects) {
      ++intersect_count;
    }
  }
  return intersect_count % 2 != 0;
}

geometry::Polygon geometry::Polygon::getIntersection(const geometry::Polygon &other) const {
  std::vector<Point> intersection_points;

  // Let's take all  the inner points first
  for (const auto &p: other.points) {
    if (isInside(p)) {
      intersection_points.emplace_back(p);
    }
  }
  for (const auto &p: this->points) {
    if (other.isInside(p)) {
      intersection_points.emplace_back(p);
    }
  }

  // Now search for intersections
  for (const auto &seg: segments) {
    for (const auto &other_seg: other.segments) {
      auto[intersects, point] = seg.intersectsWith(other_seg);
      if (intersects) {
        intersection_points.emplace_back(point);
      }
    }
  }

  return {this->id * 100 + other.id, intersection_points};
}
