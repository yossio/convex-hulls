cmake_minimum_required(VERSION 3.5)
project(homework)

if (NOT CMAKE_CXX_STANDARD)
    set(CMAKE_CXX_STANDARD 17)
endif ()

if (CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
    add_compile_options(-Wall -Wextra -Wpedantic)
endif ()


include_directories(
        include
        include/third_party
        include/third_party/SpatiumLib/include

)

find_package(OpenCV REQUIRED)

add_executable(main
        src/main.cpp
        src/geometry.cpp
        )
target_include_directories(main PUBLIC
        ${OpenCV_INCLUDE_DIRS}
        ${OpenCV_DIR})
target_link_libraries(main
        ${OpenCV_LIBS}
        )

